<?php
$fb_page_id = "1770240289752132";

error_reporting(E_ALL);
// For verification purposes
$verify_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';
if(isset($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe') {
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
    if ($hub_verify_token === $verify_token) {
        header("HTTP/1.1 200 OK");
        echo $challenge;
        die;
    }
}

// access token - facebook page
$access_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';

// Handle input from user
$input = json_decode(file_get_contents('php://input'), true);
$page_id = $input['entry'][0]['id'];
$sender_id = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;
$postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

//Trim message
$message = trim($message);
$command = explode(' ', $message);
$com1 = strtolower(array_shift($command));

$conn = database_connect();

if ($com1 && ($sender_id != $fb_page_id)) {
	  $user_id = $sender_id;

  // Messahe type and state
  //$bot_reply = false;
  $message_type = '';
  $remind_state = '';
  $remind_text = '';
  $reply = '';
	
	  if ($row) {
    // expecting reminder text
    $row['content'] = json_decode($row['content']);
    if ($row['type'] == "remind") {
      $remind_state = "save_reminder_text";
    }elseif ($row['type'] == "save_reminder_text") { // Expect time
      $remind_text = json_encode($row['content']);
      $remind_state = "save_reminder_date";
    } else {
      $remind_state = "";
    }
  }
	
	  if ($remind_state != "") {
    if ($remind_state == "save_reminder_text") {
      $message_type = "save_reminder_text";
      $reply = "When do you want to be reminded?";
    }else {
      $timestamp = strtotime($message); 
      if ($timestamp) {
        $message_type = "save_reminder_date";
        $reply = "Okay! I will remind you about this on ".date('j F Y', $timestamp)." at ".date('h:i A', $timestamp);

        // Insert into reminders database
        $add_reminder = mysql_insert('reminders', array(
          'user_id' => $user_id,
          'content' => $remind_text,
          'completed' => 0,
          'scheduled_date' => $timestamp,
          'created_at' => date("Y-m-d H:i:s"),
        ));

      } else {
        $message_type = "error";
        $reply = "You have entered an invalid date. Please try again.";
      }
    }
  } else {
    $message_type = $command;
    // Handle commands
    if ($command == "remind") {
      $reply = "What is the reminder about?";
    } elseif($command == "list") {
      $reply = "Okay, this is your current reminders.\\n";

      $reminders_query = mysql_query("SELECT content, scheduled_date FROM reminders WHERE user_id = '".$user_id."' ORDER BY scheduled_date asc");
      $cnt = 0;
      //$reply .= "SELECT content, scheduled_date FROM reminders WHERE user_id = '".$user_id."' ORDER BY scheduled_date asc";
      while($reminders = mysql_fetch_assoc($reminders_query)) {
        //$reply .= "hello";
        $reply .= "[".($cnt+1)."]".date('Y-m-d H:i', $reminders['scheduled_date'])." - ".json_decode($reminders['content'])."\\n";
        $cnt++;
      }

      // Check for no current reminders
      if ($cnt == 0) {
        $reply = "You have no active reminders set.";
      }
	
	  }

  $current_timestamp = date("Y-m-d H:i:s");
	
	
	
	
	
	
	
    switch($com1) {
        case "remind":
            $reply = $com1;
            break;
        case "list":
            $reply = $command;
            break;
        case "listen":
            $hashtag = clean_hashtag(array_shift($command));
            if ($hashtag) {
                $reply = listen($sender_id, $hashtag, $conn);
            } else {
                $reply = "Oops I don't understand, try 'LISTEN #hashtag'";
            }
            break;
        case "cancel":
            $hashtag = clean_hashtag(array_shift($command));
            if ($hashtag) {
                $reply = cancel($sender_id, $hashtag, $conn);
            } else {
                $reply = "Oops I don't understand, try 'CANCEL #hashtag'";
            }
            break;
        default:
            $reply = "Oops not in the option.";
    }

    send_reply($reply, $sender_id, $access_token);
    database_close($conn);
}









function remind($user_id) {

  // Add to reminders
}

/**
 * Get all the reminders by user
 */
function get_all_reminders_by_user($user_id) {

}

//end
/**
 * Listen to hashtags. Hashtags are not case sensitive but stored in database as lowercases.
 */
function listen($user_id, $hashtag, $db_conn) {
    // Check if the user is already subscribed to this hashtag
    $stmt = "SELECT * FROM subscriptions WHERE user_id='".$user_id."' AND hashtag='".$hashtag."'";
    $result = $db_conn->query($stmt);
    if ($result->num_rows > 0) {
        return "Already subscribed to #".$hashtag;
    }

    // New subscription
    $stmt = "INSERT INTO subscriptions (user_id, hashtag) VALUES ('".$user_id."', '".$hashtag."') ";
    if ($db_conn->query($stmt) === TRUE) {
        return "You are now listening to #".$hashtag." tweets.";
    } else {
        return "ERROR: ".$stmt." .\n".$db_conn->error;
    }
}

/**
 * Cancel hashtag
 */
function cancel($user_id, $hashtag, $db_conn) {
    // Check if the user is subscribed to this hashtag
    $stmt = "DELETE FROM subscriptions WHERE user_id='".$user_id."' AND hashtag='".$hashtag."'";
    if ($db_conn->query($stmt) === TRUE) {
        return "You are no longer listening to #".$hashtag." tweets.";
    } else {
        return "ERROR: ".$stmt." .\n";//.$db_conn->error;
    }
}

/**
 * Returns a hashtag stripped of leading "#". Returns false if there is no leading "#".
 * Hashtags case insensitive and stored as lower-case in the database.
 *
 * NOTE: Does not check for special characters or "#" after the leading "#".
 *
 * @param $hashtag string - e.g. #walangpasok
 * @return bool|string
 */
function clean_hashtag($hashtag) {
    if ($hashtag == "" || $hashtag == null || $hashtag[0] != "#") return false;
    $clean_hashtag = ltrim(strtolower($hashtag), "#");
    return ($clean_hashtag != $hashtag) ? $clean_hashtag : false;
}


/**
 * Curl function
 */
function do_curl($url, $data) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    curl_close($ch);
}

function send_reply($reply, $sender_id, $access_token) {
    // Execute reply
    $responseJSON = '{
    "recipient":{
      "id":"'. $sender_id .'"
    },
    "message": {
      "text":"'. $reply .'"
    }
  }';

    // Graph API
    $url = 'https://graph.facebook.com/v2.7/me/messages?access_token='.$access_token;

    // Send a message response
    do_curl($url, $responseJSON);
}

function database_connect() {
    $username = "root";
    $password = "1";
    $hostname = "localhost";

    //connection to the database
    $connection = new mysqli($hostname,$username, $password, "fbchatbot_db");
    if ($connection->connect_error) {
        die("Connection failed: ".$connection->connect_error);
    }
    echo "Connected<br>";

    return $connection;
}

function database_close($connection) {
    //close the connection
    $connection->close();
}

?>