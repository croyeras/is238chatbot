<?php
$fb_page_id = "1770240289752132";

//error_reporting(E_ALL);
// For verification purposes 
$verify_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';
if(isset($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe') {
  $challenge = $_REQUEST['hub_challenge'];
  $hub_verify_token = $_REQUEST['hub_verify_token'];
  if ($hub_verify_token === $verify_token) {
    header("HTTP/1.1 200 OK");
    echo $challenge;
    die;
  }
}

// access token - facebook page
$access_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';

// Handle input from user
$input = json_decode(file_get_contents('php://input'), true);
$page_id = $input['entry'][0]['id'];
$sender_id = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;
$postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;


//Trim message
$message = trim($message);
$command = explode(' ', $message);
$command = strtolower(array_shift($command));

// Connect to database
$connection = database_connect();

// Skipped reply message sent by facebook page
if ($message && ($sender_id != $fb_page_id)) {
  // User
  $user_id = $sender_id;

  // Messahe type and state
  //$bot_reply = false;
  $message_type = '';
  $remind_state = '';
  $remind_text = '';
  $reply = '';

  //$user_id = '2147483647';
  $result = mysql_query("SELECT type, content FROM messages WHERE user_id = '".$user_id."' ORDER BY created desc LIMIT 1");
  //$row1 = mysql_fetch_row($result);
  $row = mysql_fetch_assoc($result);
  $num_rows = mysql_num_rows($result);

  /*$log_message = mysql_insert('messages', array(
    'user_id' => $sender_id,
    'content' => json_encode($message),
    'type' => $row ? json_encode($row[0]) : "",
    //'type' => json_encode($input),
    'created' => date("Y-m-d H:i:s"),
  ));*/

  if ($row) {
    // expecting reminder text
    $row['content'] = json_decode($row['content']);
    if ($row['type'] == "remind") {
      $remind_state = "save_reminder_text";
    }elseif ($row['type'] == "save_reminder_text") { // Expect time
      $remind_text = json_encode($row['content']);
      $remind_state = "save_reminder_date";
    } else {
      $remind_state = "";
    }
  }

  //die();
  // Handling reminder states
  if ($remind_state != "") {
    if ($remind_state == "save_reminder_text") {
      $message_type = "save_reminder_text";
      $reply = "When do you want to be reminded?";
    }else {
      $timestamp = strtotime($message); 
      if ($timestamp) {
        $message_type = "save_reminder_date";
        $reply = "Okay! I will remind you about this on ".date('j F Y', $timestamp)." at ".date('h:i A', $timestamp);

        // Insert into reminders database
        $add_reminder = mysql_insert('reminders', array(
          'user_id' => $user_id,
          'content' => $remind_text,
          'completed' => 0,
          'scheduled_date' => $timestamp,
          'created_at' => date("Y-m-d H:i:s"),
        ));

      } else {
        $message_type = "error";
        $reply = "You have entered an invalid date. Please try again.";
      }
    }
  } else {
    $message_type = $command;
    // Handle commands
    if ($command == "remind") {
      $reply = "What is the reminder about?";
    } elseif($command == "list") {
      $reply = "Okay, this is your current reminders.\\n";

      $reminders_query = mysql_query("SELECT content, scheduled_date FROM reminders WHERE user_id = '".$user_id."' ORDER BY scheduled_date asc");
      $cnt = 0;
      //$reply .= "SELECT content, scheduled_date FROM reminders WHERE user_id = '".$user_id."' ORDER BY scheduled_date asc";
      while($reminders = mysql_fetch_assoc($reminders_query)) {
        //$reply .= "hello";
        $reply .= "[".($cnt+1)."]".date('Y-m-d H:i', $reminders['scheduled_date'])." - ".json_decode($reminders['content'])."\\n";
        $cnt++;
      }

      // Check for no current reminders
      if ($cnt == 0) {
        $reply = "You have no active reminders set.";
      }
      
    } elseif($command == "listen") {
      $reply = "Listen to tweets";
    } elseif($command == "cancel") {
      $reply = "Cancel tweets or reminders";
    } else {
      $message_type = "error";
      $reply = "Oops not in the option.";
    }
  }

  $current_timestamp = date("Y-m-d H:i:s");

  $log_message = mysql_insert('messages', array(
    'user_id' => $user_id,
    'content' => json_encode($message),
    'type' => $message_type,
    //'type' => json_encode($row1[0]),
    'created' => $current_timestamp,
  ));

  // Execute reply
  $responseJSON = '{
    "recipient":{
      "id":"'. $sender_id .'"
    },
    "message": {
      "text":"'. $reply.'"
    }
  }';

  // Graph API
  $url = 'https://graph.facebook.com/v2.7/me/messages?access_token='.$access_token;

  // Send a message response
  do_curl($url, $responseJSON);
}

function sendreply($reply, $sender_id, $access_token) {
  // Execute reply
  $responseJSON = '{
    "recipient":{
      "id":"'. $sender_id .'"
    },
    "message": {
      "text":"'. $reply .'"
    }
  }';

  // Graph API
  $url = 'https://graph.facebook.com/v2.7/me/messages?access_token='.$access_token;

  // Send a message response
  do_curl($url, $responseJSON);
}

/**
 * Remind user
 */
function remind($user_id) {

  // Add to reminders
}

/**
 * Get all the reminders by user
 */
function get_all_reminders_by_user($user_id) {

}

/**
 * Listen to hashtags
 */
function listen($user_id, $hashtag) {

}

/**
 * Cancel hashtag
 */
function cancel($user_id, $hashtag) {

}

/**
 * Curl function
 */
function do_curl($url, $data) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  $result = curl_exec($ch);
  curl_close($ch);
}


function database_connect() {
  $username = "prj_healthhub";
  $password = "prj123health";
  $hostname = "mysql.claireroyeras.com"; 

  //connection to the database
  $connection = mysql_connect($hostname, $username, $password) 
    or die("Unable to connect to MySQL");
  echo "Connected";

  // Select db
  $select = mysql_select_db("healthhub_database", $connection) 
  or die("Could not select fbchatbot_db");
}

function database_close($connection) {
  //close the connection
  mysql_close($connection);
}

function mysql_insert($table, $inserts) {
  $values = array_map('mysql_real_escape_string', array_values($inserts));
  $keys = array_keys($inserts);
      
  return mysql_query('INSERT INTO `'.$table.'` (`'.implode('`,`', $keys).'`) VALUES (\''.implode('\',\'', $values).'\')');
}

?>