<?php
$fb_page_id = "1770240289752132";

error_reporting(E_ALL);
// For verification purposes
$verify_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';
if(isset($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe') {
    $challenge = $_REQUEST['hub_challenge'];
    $hub_verify_token = $_REQUEST['hub_verify_token'];
    if ($hub_verify_token === $verify_token) {
        header("HTTP/1.1 200 OK");
        echo $challenge;
        die;
    }
}

// access token - facebook page
$access_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';

// Handle input from user
$input = json_decode(file_get_contents('php://input'), true);
$page_id = $input['entry'][0]['id'];
$sender_id = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;
$postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

//Trim message
$message = trim($message);
$command = explode(' ', $message);
$com1 = strtolower(array_shift($command));

$conn = database_connect();

if ($com1 && ($sender_id != $fb_page_id)) {
    switch($com1) {
        case "remind":
            $reply = $com1;
            break;
        case "list":
            $reply = $command;
            break;
        case "listen":
            $hashtag = clean_hashtag(array_shift($command));
            if ($hashtag) {
                $reply = listen($sender_id, $hashtag, $conn);
            } else {
                $reply = "Oops I don't understand, try 'LISTEN #hashtag'";
            }
            break;
        case "cancel":
            $hashtag = clean_hashtag(array_shift($command));
            if ($hashtag) {
                $reply = cancel($sender_id, $hashtag, $conn);
            } else {
                $reply = "Oops I don't understand, try 'CANCEL #hashtag'";
            }
            break;
        default:
            $reply = "Oops not in the option.";
    }

    send_reply($reply, $sender_id, $access_token);
    database_close($conn);
}

/**
 * Listen to hashtags. Hashtags are not case sensitive but stored in database as lowercases.
 */
function listen($user_id, $hashtag, $db_conn) {
    // Check if the user is already subscribed to this hashtag
    $stmt = "SELECT * FROM subscriptions WHERE user_id='".$user_id."' AND hashtag='".$hashtag."'";
    $result = $db_conn->query($stmt);
    if ($result->num_rows > 0) {
        return "Already subscribed to #".$hashtag;
    }

    // New subscription
    $stmt = "INSERT INTO subscriptions (user_id, hashtag) VALUES ('".$user_id."', '".$hashtag."') ";
    if ($db_conn->query($stmt) === TRUE) {
        return "You are now listening to #".$hashtag." tweets.";
    } else {
        return "ERROR: ".$stmt." .\n".$db_conn->error;
    }
}

/**
 * Cancel hashtag
 */
function cancel($user_id, $hashtag, $db_conn) {
    // Check if the user is subscribed to this hashtag
    $stmt = "DELETE FROM subscriptions WHERE user_id='".$user_id."' AND hashtag='".$hashtag."'";
    if ($db_conn->query($stmt) === TRUE) {
        $stmt = "DELETE FROM notifications WHERE user_id='".$user_id."' AND hashtag='".$hashtag."'";
        if ($db_conn->query($stmt) === TRUE) {
            return "You are no longer listening to #".$hashtag." tweets.";
        } else {
            return "ERROR: ".$stmt." .\n";//.$db_conn->error;
        }
    } else {
        return "ERROR: ".$stmt." .\n";//.$db_conn->error;
    }
}

/**
 * Returns a hashtag stripped of leading "#". Returns false if there is no leading "#".
 * Hashtags case insensitive and stored as lower-case in the database.
 *
 * NOTE: Does not check for special characters or "#" after the leading "#".
 *
 * @param $hashtag string - e.g. #walangpasok
 * @return bool|string
 */
function clean_hashtag($hashtag) {
    if ($hashtag == "" || $hashtag == null || $hashtag[0] != "#") return false;
    $clean_hashtag = ltrim(strtolower($hashtag), "#");
    return ($clean_hashtag != $hashtag) ? $clean_hashtag : false;
}


/**
 * Curl function
 */
function do_curl($url, $data) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    curl_close($ch);
}

function send_reply($reply, $sender_id, $access_token) {
    // Execute reply
    $responseJSON = '{
    "recipient":{
      "id":"'. $sender_id .'"
    },
    "message": {
      "text":"'. $reply .'"
    }
  }';

    // Graph API
    $url = 'https://graph.facebook.com/v2.7/me/messages?access_token='.$access_token;

    // Send a message response
    do_curl($url, $responseJSON);
}

function database_connect() {
    $username = "dinia";
    $password = "G3pt31$238";
    $hostname = "localhost";

    //connection to the database
    $connection = new mysqli($hostname,$username, $password, "fbchatbot_db");
    if ($connection->connect_error) {
        die("Connection failed: ".$connection->connect_error);
    }
    echo "Connected<br>";

    return $connection;
}

function database_close($connection) {
    //close the connection
    $connection->close();
}

?>