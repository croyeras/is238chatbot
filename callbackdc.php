<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);
date_default_timezone_set('Asia/Manila');
$fb_page_id = "1770240289752132";

// For verification purposes 
$verify_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';
if(isset($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe') {
  $challenge = $_REQUEST['hub_challenge'];
  $hub_verify_token = $_REQUEST['hub_verify_token'];
  if ($hub_verify_token === $verify_token) {
    header("HTTP/1.1 200 OK");
    echo $challenge;
    die;
  }
}

// access token - facebook page
$access_token = 'EAAHuGgGPMpkBAHnK9aKRWC0BP5rVAoPO8IPhmJmsd0lCow50LZCF37ge5wRaR8x19qStheIepSaZCBKHJGbLnGN0b9KUGTSAgHEpFNuyVngZBGULXagaA8NZBamIEIOidZBQcsclc05kZB9NYBG3ZAUEiW2h6vJfuem929qBO2c7QZDZD';

// Handle input from user
$input = json_decode(file_get_contents('php://input'), true);
$page_id = $input['entry'][0]['id'];
$sender_id = $input['entry'][0]['messaging'][0]['sender']['id'];
$message = isset($input['entry'][0]['messaging'][0]['message']['text']) ? $input['entry'][0]['messaging'][0]['message']['text']: '' ;
$postback = isset($input['entry'][0]['messaging'][0]['postback']['payload']) ? $input['entry'][0]['messaging'][0]['postback']['payload']: '' ;

//Trim message
$message = trim($message);
$command = explode(' ', $message);
$com1 = strtolower(array_shift($command));

// Connect to database
$conn = database_connect();

// For CRON
if (isset($_GET['run'])) {
  $run = $_GET['run'];
  if($run == "send_reminders") {
    run_send_reminders($access_token, $conn);
  }elseif($run == "send_tweets") {
    run_send_tweets($access_token, $conn);
  }
  database_close($conn);
  die;
}

// Skipped reply message sent by facebook page
if ($message && ($sender_id != $fb_page_id)) {
  $user_id = $sender_id;
  $message_type = $com1;
  $remind_state = '';
  $remind_text = '';
  $reply =  '';

  // Check for remind state
  $stmt = $conn->prepare("SELECT type, content FROM messages WHERE user_id = ? ORDER BY created desc LIMIT 1");
  $stmt->bind_param("s", $user_id);
  $stmt->execute();
  $result = $stmt->get_result();
  $row = $result->fetch_assoc();

  if ($result->num_rows > 0) {
    $row['content'] = json_decode($row['content']);
    if ($row['type'] == "remind") {
      $remind_state = "save_reminder_text";
    }elseif ($row['type'] == "save_reminder_text") { // Expect time
      $remind_text = json_encode($row['content']);
      $remind_state = "save_reminder_date";
    } else {
      $remind_state = "";
    }
  }

  // Handling reminder states
  if ($remind_state != "") {
    if ($remind_state == "save_reminder_text") {
      $message_type = "save_reminder_text";
      $reply = "When do you want to be reminded?";
    }else {
      $timestamp = strtotime($message); 
      if ($timestamp) {
        $message_type = "save_reminder_date";
        $reply = "Okay! I will remind you about this on ".date('j F Y', $timestamp)." at ".date('h:i A', $timestamp);

        $reminder_data = array(
          'user_id' => $user_id,
          'content' => $remind_text,
          'completed' => 0,
          'scheduled_date' => $timestamp,
          'created_at' => date("Y-m-d H:i:s"),
        );

        // Insert into reminders table
        add_reminder($user_id, $reminder_data, $conn);
      } else {
        $message_type = "error";
        $reply = "You have entered an invalid date. Please reply with 'remind' if you wish to redo the process.";
      }
    }
  } else {
    $message_type = $com1;

    // Handling commands
    switch($com1) {
      case "remind":
        $reply = "What is the reminder about?";
        break;
      case "list":
        $reply = get_all_reminders_by_user($user_id, $conn);
        break;
      case "listen":
        $hashtag = clean_hashtag(array_shift($command));
        if ($hashtag) {
          $reply = listen($sender_id, $hashtag, $conn);
        } else {
          $reply = "Oops I didn't catch that, try 'LISTEN #hashtag'";
        }
        break;
      case "cancel":
        // Handle cancel from reminders and tweets
        $com2 = array_shift($command);
        if (is_numeric($com2)) {
          // Handle cancel reminder
          $reply = remove_reminder_by_user($user_id, $com2, $conn);
        } else {
          $hashtag = clean_hashtag($com2);
          if ($hashtag) {
            $reply = cancel($sender_id, $hashtag, $conn);
          } else {
            $reply = "Oops I didn't catch that, try 'CANCEL #hashtag'";
          }
        }
        break;
      default:
        $reply = "Hello! What would you like to accomplish today?\\nWould you like to set a reminder? If so, please reply with 'remind'.\\nOtherwise, if you would like to listen to tweets with matching hashtags, please reply with 'listen' followed with the hashtag.";
    }
  }

  // Log messages
  $current_timestamp = date("Y-m-d H:i:s");
  $log_data = array(
    'user_id' => $user_id,
    'content' => json_encode($message),
    'type' => $message_type,
    'created' => $current_timestamp,
  ); 
  log_message($sender_id, $log_data, $conn);

  // Execute reply
  send_reply($reply, $sender_id, $access_token);
  database_close($conn);
}


/**
 * Get all the reminders by user
 * @param  [type] $user_id [description]
 * @return [type]          [description]
 */
function get_all_reminders_by_user($user_id, $db_conn) {
  $reply = "";
  $completed = 0;
  $stmt = $db_conn->prepare("SELECT content, scheduled_date FROM reminders WHERE user_id = ? AND completed = ? ORDER BY scheduled_date asc");
  $stmt->bind_param("si", $user_id, $completed);
  $stmt->execute();
  $result = $stmt->get_result();

  if ($result->num_rows > 0) {
    $reply = "Okay, here are your current reminders.\\n";
    $cnt = 1;
    while ($row = $result->fetch_assoc()) {
      $reply .= "[".$cnt."] ".date('Y-m-d H:i', $row['scheduled_date'])." - ".json_decode($row['content'])."\\n";
      $cnt++;
    }
    $reply .= "To cancel reminders, reply with 'CANCEL' along with the reminder number. (e.g. CANCEL 1)";
  } else {
    $reply = "You have no active reminders set.";
  }
  return $reply;
}

/**
 * [remove_reminder_by_user description]
 * @param  [type] $user_id [description]
 * @param  [type] $db_conn [description]
 * @return [type]          [description]
 */
function remove_reminder_by_user($user_id, $list_no, $db_conn) {
  $reply = "Unable to cancel the reminder. Please wait while we fix this.";
  $stmt = $db_conn->prepare("SELECT id, content, scheduled_date FROM reminders WHERE user_id = ? AND completed = 0 ORDER BY scheduled_date asc");
  $stmt->bind_param("s", $user_id);
  $stmt->execute();
  $result = $stmt->get_result();

  $reminder_id = false; 
  if ($list_no) {
    $cnt = 1;
    while ($row = $result->fetch_assoc()) {
      if ($cnt == $list_no) {
        $reminder_id = $row['id'];
        break;
      }
      $cnt++;
    }
    if ($reminder_id) {
      $reply = "Alright, cancelled your reminder about ".json_decode($row['content'])." -  ".date('j F Y H:i A', $row['scheduled_date']);

      // Proceed to delete
      delete_reminder($user_id, $reminder_id, $db_conn);
    } else {
      $reply = "The reminder does not exist. Did you enter the correct reminder number?";
    }
    
  }

  return $reply;
}
/**
 * Add reminder
 * @param [type] $user_id [description]
 * @param [type] $data    [description]
 * @param [type] $db_conn [description]
 */
function add_reminder($user_id, $data, $db_conn) {
  $reminder = $db_conn->prepare("INSERT INTO reminders (user_id, content, completed, scheduled_date, created_at) VALUES (?,?,?,?,?)");
  $reminder->bind_param("ssiis", $data['user_id'], $data['content'], $data['completed'], $data['scheduled_date'], $data['created_at']);
  $reminder->execute();
}

function delete_reminder($user_id, $reminder_id, $db_conn) {
  $stmt = $db_conn->prepare("DELETE FROM reminders WHERE user_id = ? AND id = ?");
  $stmt->bind_param("si", $user_id, $reminder_id);
  $stmt->execute();
  $result = $stmt->get_result();
  return $result;
}

/**
 * Log messsages
 */
function log_message($user_id, $data, $db_conn) {
  $log = $db_conn->prepare("INSERT INTO messages (user_id, content, type, created) VALUES (?,?,?,?)");
  $log->bind_param("ssss", $user_id, $data['content'], $data['type'], $data['created']);
  $log->execute();
}

/**
 * Listen to hashtags. Hashtags are not case sensitive but stored in database as lowercases.
 */
function listen($user_id, $hashtag, $db_conn) {
    // Check if the user is already subscribed to this hashtag
    $stmt = $db_conn->prepare("SELECT * FROM subscriptions WHERE user_id=? AND hashtag=?");
    $stmt->bind_param("ss", $user_id, $hashtag);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows > 0) {
        return "You are already subscribed to #".$hashtag;
    }

    // New subscription
    $stmt = $db_conn->prepare("INSERT INTO subscriptions (user_id, hashtag) VALUES (?,?) ");
    $stmt->bind_param("ss", $user_id, $hashtag);
    if ($stmt->execute() === TRUE) {
        return "You are now listening to #".$hashtag." tweets. To stop listening on tweets with specific hashtags, please reply with 'cancel' followed with the hashtag (example: cancel #love).";
    } else {
        return "ERROR: ".$stmt." .\n".$db_conn->error;
    }
}

/**
 * Cancel hashtag
 */
function cancel($user_id, $hashtag, $db_conn) {
    // Check if the user is subscribed to this hashtag
    $stmt = $db_conn->prepare("SELECT * FROM subscriptions WHERE user_id=? AND hashtag=?");
    $stmt->bind_param("ss", $user_id, $hashtag);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 0) {
      return "You are not subscribed to #".$hashtag;
    }

    $stmt = $db_conn->prepare("DELETE FROM subscriptions WHERE user_id=? AND hashtag=?");
    $stmt->bind_param("ss", $user_id, $hashtag);
    if ($stmt->execute() === TRUE) {
        $stmt->close();
        $stmt = $db_conn->prepare("DELETE FROM notifications WHERE user_id=? AND hashtag=?");
        $stmt->bind_param("ss", $user_id, $hashtag);
        if ($stmt->execute() === TRUE) {
            return "You are no longer listening to #".$hashtag." tweets.";
        } else {
            return "ERROR: ".$stmt." .\n";//.$db_conn->error;
        }
    } else {
        return "ERROR: ".$stmt." .\n";//.$db_conn->error;
    }
}

/**
 * Returns a hashtag stripped of leading "#". Returns false if there is no leading "#".
 * Hashtags case insensitive and stored as lower-case in the database.
 *
 * NOTE: Does not check for special characters or "#" after the leading "#".
 *
 * @param $hashtag string - e.g. #walangpasok
 * @return bool|string
 */
function clean_hashtag($hashtag) {
    if ($hashtag == "" || $hashtag == null || $hashtag[0] != "#") return false;
    $clean_hashtag = ltrim(strtolower($hashtag), "#");
    return ($clean_hashtag != $hashtag) ? $clean_hashtag : false;
}

/**
 * Send reminders to users
 */
function run_send_reminders($access_token, $db_conn) {
  $stmt = "SELECT id, user_id, content, scheduled_date FROM reminders WHERE scheduled_date <= UNIX_TIMESTAMP() AND completed = 0 LIMIT 100";
  $result = $db_conn->query($stmt);

  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      // Update reminders
      $stmt2 = $db_conn->prepare("UPDATE reminders SET completed = 1 WHERE id=?");
      $stmt2->bind_param("i", $row['id']);
      $complete = $stmt2->execute();

      // Send notification
      $notification = "REMINDER ALERT: ".json_decode($row['content'])." -  ".date('Y-m-d H:i A', $row['scheduled_date']);
      send_reply($notification, $row['user_id'], $access_token);
    }
  }
}

/**
 * Send tweets to users
 */
function run_send_tweets($access_token, $db_conn) {
  $sent_notif_ids = array();

// Get notifications that are not yet sent, store id, and join with `tweets` table.
// Only send the earliest tweet not sent per user, every time this module is executed.
  $stmt = "SELECT notifications.id as notif_id, notifications.twitter_id as twitter_id, user_id, tweets.url, tweets.text, hashtag FROM notifications
    INNER JOIN tweets ON tweets.twitter_id=notifications.twitter_id WHERE sent=0 GROUP BY user_id ORDER BY twitter_id ASC";
  $result = $db_conn->query($stmt);
  if ($result->num_rows > 0) {
      // For each tweet_id, send to user
      while ($row = $result->fetch_assoc()) {
          // Remove the twitter-generated URL at the end
  //        $temp = explode(' ', $row['text']);
  //        array_pop($temp);
  //        $temp = implode(" ", $temp);

          $bot_notif = "A tweet matched #". $row['hashtag'] . ": " . $row['text'] . " (" . $row['url'] . ")";
          send_reply($bot_notif, $row['user_id'], $access_token);
          array_push($sent_notif_ids, (int)($row['notif_id']));
      }
  }
  $result->free();

  // Mark notification ids as sent
  foreach ($sent_notif_ids as $id) {
      $stmt = "UPDATE notifications SET sent=1, sent_at=now() WHERE id=" . $id;
      if ($db_conn->query($stmt) === TRUE) {
          echo "Record updated successfully<br>";
      } else {
          echo "Error updating record: " . $db_conn->error . "<br>";
      }
  }
}

/**
 * Curl function
 */
function do_curl($url, $data) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $result = curl_exec($ch);
    curl_close($ch);
}

function send_reply($reply, $sender_id, $access_token) {
    // Execute reply
    $responseJSON = '{
    "recipient":{
      "id":"'. $sender_id .'"
    },
    "message": {
      "text":"'. $reply .'"
    }
  }';

    // Graph API
    $url = 'https://graph.facebook.com/v2.7/me/messages?access_token='.$access_token;

    // Send a message response
    do_curl($url, $responseJSON);
}

function database_connect() {
  $username = "root";
  $password = "1$238_Gr0up2!";
  $hostname = "localhost";

  //connection to the database
  $connection = new mysqli($hostname,$username, $password, "fbchatbot_db");
  if ($connection->connect_error) {
      die("Connection failed: ".$connection->connect_error);
  }
  echo "Connected<br>";

  return $connection;
}

function database_close($connection) {
    //close the connection
    $connection->close();
}

?>
