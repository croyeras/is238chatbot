# Facebook Chatbot
This is a simple chatbot app using Facebook [Webhook Events](https://developers.facebook.com/docs/messenger-platform/reference/webhook-events/messages)
to send and receive messages between a Facebook user and a bot (hosted in an AWS EC2 instance).

## Features

#### Reminders
The bot is able to remind a Facebook user on time (up to 1 minute accuracy). 
The user is able to list all the reminders and cancel them when desired.

Example:
```
User: REMIND
Bot Response: What is the reminder about?
User: Pay Internet bill
Bot Response: When do you want to be reminded?
User: tomorrow 5pm
Bot Response: Okay! I will remind you about this on 28 October 2018 at 5:00 PM.

User: REMIND
Bot Response: What is the reminder about?
User: Buy dog food
Bot Response: When do you want to be reminded?
User: next thursday 3pm
Bot Response: Okay! I will remind you about this on 1 November 2018 at 3:00 PM.

User: LIST
Bot Response: [1] 2018-10-28 17:00 - Pay Internet bill
              [2] 2018-11-01 15:00 - Buy dog food
User: CANCEL 2
Bot Response: Canceled reminder about "Buy dog food" on 1 November 2018 3:00 PM.
```

#### Twitter Notifications
The user is able to subscribe and listen to a specific hashtag. When there is a tweet
containing the hashtag, the user is notified. The user can also unsubscribe to a hashtag.

Example:
```
User: LISTEN #walangpasok
Bot Response: You are now listening to #walangpasok tweets.
(When someone tweets #walangpasok)
Bot Message: A tweet matched #walangpasok: (copy of tweet) (URL of tweet)

User: CANCEL #walangpasok
Bot Response: You are no longer listening to #walangpasok tweets.
```

## Setup

### Requirements
- AWS EC2 instance with LAMP server and SSL
- [Facebook Page](https://www.facebook.com/is238chatter/)
- [Facebook developer account](https://developers.facebook.com/apps/543270439432857/dashboard/)
- Twitter developer account

### Server
#### Initial server setup
1. [Login](https://awseducate.qwiklabs.com/users/sign_in?locale=en) to your AWS Educate account in Qwiklabs.
2. Click on "AWS Educate Starter Account 75".
3. Make sure you have already started the lab.
4. Click “Open Console”. You should have access to the “AWS Management Console”.
5. [Launch](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html?icmpid=docs_ec2_console) 
an Elastic Compute Cloud (EC2) instance. EC2 instances are virtual machines.
**NOTE​​:** Skip the third step in that page (Clean up your instance) because we 
want this instance to continue running.
6. [Install Linux, Apache, MySQL, and PHP](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html)
7. [Upload](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html#AccessingInstancesLinuxSCP) your website into the corresponding htdocs directory.
#### Enabling SSL certificate
Facebook webhooks require your domain to be encrypted. To do this in an AWS instance,
[follow these steps](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/SSL-on-an-instance.html#letsencrypt).
#### Adding server users
To add users, the public keys of the users' machines must be handed to the system administrator for authentication. The steps in adding users are as follows:

1. Execute command 'sudo adduser user_name' replacing user_name with the desired username.
2. Optional - to set a password, the command 'sudo passwd user_name' must be executed.
3. After the user is created, navigation to the user folder should be done as root (e.g. cd /home/user_name).
4. Once the directory has been located, the .ssh directory should then be navigated to. If the directory does not exist, create one (mkdir .ssh).
5. Once  inside the directory, create a blank file named 'authorized_keys' (nano authorized_keys) and paste the public key of the user's machine in it.
6. Save the file then execute the command 'chown -R user_name:user_name /home/user_name/.ssh' to provide ownership to the user.
7. To allow reading and writing permissions for the owner on the .ssh file, the command 'chmod 600 /home/user_name/.ssh/authorized_keys' must be executed.
8. Lastly, to allow reading, writing, and execution permissions for the owner on the .ssh folder, the command 'chmod 700 /home/user_name/.ssh' must then be executed.
9. To test, run the command 'ssh user_name@host_ip' in the terminal, replacing host_ip with the server's ip address or domain name.

## Development
### SSH to server
As a registered server user you may SSH to the server via CLI, for example
```
$ ssh dinia@salazarcv.tk
```
### Database
MySQL is the database of choice for the chatbot app.

### Allowing remote access to MySQL Database on the instance
In order to allow remote access to the database on the instance from a local machine, the security group must first be set to allow mysql connections to port 3306.

1. Login to the AWS EC2 Console.
2. Navigate to the instances.
3. Select the instance.
4. Click on view inbound rules on the instance description pane.
5. Add a new rule specifying MySQL on the first column, and 'Anywhere' on the source.
6. Click on 'Save'
7. Connect to the server through SSH.
8. Edit the /etc/my.cnf file and enter the following on succeeding lines from the [mysqld] label: 'bind-address=0.0.0.0'
9. Save the file.
10. Attempt to remotely access the MySQL database through the terminal with the command 'mysql -u username -p -h host_address'.

#### Login credentials
- Username: 
- Password:
#### Dump
TODO

## Testing
1. Upload `callback.php` to an AWS hosted site with SSL enabled.
2. Acquire admin access to our Facebook page (need an invite from repo owner).
3. Go to Facebook developer's site and access the app (need an invite from repo owner).
4. Go to "Webhooks" tab and change the URL to domain with SSL (e.g. mysite.com/callback.php)
