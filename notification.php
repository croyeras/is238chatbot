<?php
require "callbackdc.php";

$sent_notif_ids = array();

// Get notifications that are not yet sent, store id, and join with `tweets` table.
// Only send the earliest tweet not sent per user, every time this module is executed.
$stmt = "SELECT notifications.id as notif_id, notifications.twitter_id as twitter_id, user_id, tweets.url, tweets.text, hashtag FROM notifications
  INNER JOIN tweets ON tweets.twitter_id=notifications.twitter_id WHERE sent=0 GROUP BY user_id ORDER BY twitter_id ASC";
$result = $conn->query($stmt);
if ($result->num_rows > 0) {
    // For each tweet_id, send to user
    while ($row = $result->fetch_assoc()) {
        // Remove the twitter-generated URL at the end
//        $temp = explode(' ', $row['text']);
//        array_pop($temp);
//        $temp = implode(" ", $temp);

        $bot_notif = "A tweet matched #". $row['hashtag'] . ": " . $row['text'] . " (" . $row['url'] . ")";
        send_reply($bot_notif, $row['user_id'], $access_token);
        array_push($sent_notif_ids, (int)($row['notif_id']));
    }
}
//$result->free();

// Mark notification ids as sent
foreach ($sent_notif_ids as $id) {
    $stmt = "UPDATE notifications SET sent=1, sent_at=now() WHERE id=" . $id;
    if ($conn->query($stmt) === TRUE) {
        echo "Record updated successfully<br>";
    } else {
        echo "Error updating record: " . $conn->error . "<br>";
    }
}

database_close($conn);
