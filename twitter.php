<?php
require "vendor/autoload.php";
require "callbackdc.php";

use Abraham\TwitterOAuth\TwitterOAuth;

define("CONSUMER_KEY", "yY55RpJSMqbbzBRjSBlHwTOpr");
define("CONSUMER_SECRET", "D42UbYBm7kgtpOw4QwNnEpopU320puXPVfWf93cCdnFSnewTqK");
$tw_access_token = "936177532226887680-qT1j39ktZCRHgIlW5ZYQiAGTUmVSfb9";
$tw_access_token_secret = "ACLgwtNQiZJbBQAcRlmBp9I8p9q1ehfhDbEQAdqTQnDjV";

$tw_connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $tw_access_token, $tw_access_token_secret);
$tw_connection->setTimeouts(10, 15);
$tw_url = $tw_connection->url("oauth/authorize", ["oauth_token" => "936177532226887680-qT1j39ktZCRHgIlW5ZYQiAGTUmVSfb9"]);

$notifs_to_add = array();
$notifs_to_add = populate_tweets_table($conn, $tw_connection, $notifs_to_add);
populate_notifications_table($conn, $notifs_to_add);

function populate_tweets_table ($conn, $tw_connection, $notifs_to_add) {
    $tweet_ids = array();
    $tweets_to_add = array();
    $user_hashtags = array();

    // Get tweet ids stored in the database
    $stmt = "SELECT twitter_id FROM tweets";
    $result = $conn->query($stmt);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_row()) {
            array_push($tweet_ids, $row[0]);
        }
    }
    $result->free();

    // Get users and their hashtag subscription
    $stmt = "SELECT user_id, group_concat(hashtag) as hashtags FROM subscriptions GROUP BY user_id";
    $result = $conn->query($stmt);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $hashtags = explode(',', $row['hashtags']);
            array_push($user_hashtags, array("user_id"=>$row['user_id'], "hashtags"=>$hashtags));
        }
    }
    $result->free();

    // Get subscribed hashtags by users
    $stmt = "SELECT DISTINCT hashtag FROM subscriptions";
    $result = $conn->query($stmt);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_row()) {
            $hashtag = $row[0];
            $content = $tw_connection->get("search/tweets", array("q" => "%23" . $hashtag, "result_type", "recent"));  // TODO: can add `since_id` to only get tweets from a specific timeline
            foreach ($content->statuses as $tweet) {
                $tweet_id = $tweet->id;
                $text = $tweet->text;
                $url = "https://twitter.com/" . $tweet->user->screen_name . "/status/" . $tweet_id;
                $text_arr = explode(' ', $text);
                // TODO: decide if get twitter-supplied `created_at` field instead of using local time to populate `tweets.created_at`

                // Check if it's a new tweet, ignore if it's a `retweet` as well
                if (!in_array($tweet_id, $tweet_ids) && (array_shift($text_arr) != "RT")) {
                    array_push($tweets_to_add, array("twitter_id"=>$tweet_id, "url"=>$url, "text"=>$text));
                    // Also add this new tweet to the notifications list with the corresponding user subscribed to it
                    foreach ($user_hashtags as $user_subscription) {
                        if (in_array($hashtag, $user_subscription['hashtags'])) {
                            array_push($notifs_to_add, array(
                                "twitter_id" => $tweet_id,
                                "hashtag" => $hashtag,
                                "user_id" => $user_subscription['user_id'])
                            );
                        }
                    }

                }
            }
        }
    }
    $result->free();

    // Add new tweets in database using bind to prevent SQL injection
    try {
        $stmt = $conn->prepare("INSERT INTO tweets (twitter_id, url, text) VALUES (?, ?, ?)");
        foreach ($tweets_to_add as $tweet) {
            $stmt->bind_param("sss", $twitter_id, $url, $text);
            $twitter_id = $tweet['twitter_id'];
            $url = $tweet['url'];
            $text = $tweet['text'];
            $stmt->execute();
            echo "Tweet added successfully<br>";
        }
        $stmt->close();
    } catch (mysqli_sql_exception $e) {
        echo "Error: ". $e->getMessage() . "<br>";
    }

    return $notifs_to_add;
}

function populate_notifications_table($conn, $notifs_to_add) {
    try {
        $stmt = $conn->prepare("INSERT INTO notifications (twitter_id, hashtag, user_id) VALUES (?, ?, ?)");
        foreach ($notifs_to_add as $notif) {
            $stmt->bind_param("sss", $twitter_id, $hashtag, $user_id);
            $twitter_id = $notif['twitter_id'];
            $hashtag = $notif['hashtag'];
            $user_id = $notif['user_id'];
            $stmt->execute();
            echo "Notification added successfully<br>";
        }
        $stmt->close();
    } catch (mysqli_sql_exception $e) {
        echo "Error: ". $e->getMessage() . "<br>";
    }
}

database_close($conn);


/*echo "https://twitter.com/".$content->statuses[0]->user->screen_name."/status/".$content->statuses[0]->id;

echo "<pre>";
print_r($content);
echo "</pre>";*/
